use number_traits::Num;

use super::zero;

#[inline]
pub fn sdiv<'a, T: Copy + Num>(out: &'a mut [T; 16], a: &[T; 16], s: T) -> &'a mut [T; 16] {
    if s == T::zero() {
        zero(out)
    } else {
        out[0] = a[0] / s;
        out[1] = a[1] / s;
        out[2] = a[2] / s;
        out[3] = a[3] / s;
        out[4] = a[4] / s;
        out[5] = a[5] / s;
        out[6] = a[6] / s;
        out[7] = a[7] / s;
        out[8] = a[8] / s;
        out[9] = a[9] / s;
        out[10] = a[10] / s;
        out[11] = a[11] / s;
        out[12] = a[12] / s;
        out[13] = a[13] / s;
        out[14] = a[14] / s;
        out[15] = a[15] / s;
        out
    }
}
#[test]
fn test_sdiv() {
    let mut v = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
    sdiv(&mut v, &[1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1], 1);
    assert!(v == [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
}
